package com.swagger.compare;
import com.deepoove.swagger.diff.SwaggerDiff;

public class Response {
    SwaggerDiff greetings;

    public SwaggerDiff getGreetings() {
        return greetings;
    }

    public void setGreetings(SwaggerDiff greetings) {
        this.greetings = greetings;
    }

    public Response(SwaggerDiff greetings) {
        this.greetings = greetings;
    }

    public Response() {
    }
}