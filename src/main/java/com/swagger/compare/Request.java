package com.swagger.compare;

public class Request {
    String urlOld;
    String urlNew;

    public String getUrlOld() {
        return urlOld;
    }

    public void seturlOld(String urlOld) {
        this.urlOld = urlOld;
    }

    public String getUrlNew() {
        return urlNew;
    }

    public void seturlNew(String urlNew) {
        this.urlNew = urlNew;
    }

    public Request(String urlOld, String urlNew) {
        this.urlOld = urlOld;
        this.urlNew = urlNew;
    }

    public Request() {
    }
}