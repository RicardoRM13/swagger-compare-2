package com.swagger.compare;

import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.deepoove.swagger.diff.SwaggerDiff; 

public class SwaggerComp implements RequestHandler<Request, Response> {
    
    public Response handleRequest(Request request, Context context) {
        //SwaggerDiff swg = null;
        return new Response(SwaggerDiff.compareV2(request.urlOld,request.urlNew));
    }
}